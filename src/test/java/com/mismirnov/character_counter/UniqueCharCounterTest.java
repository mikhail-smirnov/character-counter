package com.mismirnov.character_counter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import com.mismirnov.character_counter.counter.UniqueCharCounter;
import java.util.LinkedHashMap;
import java.util.Map;

public class UniqueCharCounterTest {

    private UniqueCharCounter uniqueCharCounter;

    @Before
    public void setUp() {
        uniqueCharCounter = new UniqueCharCounter();
    }


    @Test(expected = IllegalArgumentException.class)
    public void givenEmptyString_whenCountCharacter_thenNullPointerExceptionThrown() {
        String inputString = null;
        uniqueCharCounter.countCharacters(inputString);
    }


    @Test
    public void givenLongText_whenCountCharacter_thenNumberOfUniqueCharactersInStringReturned() {
        String inputString = "Get out Get out Get out Get out Get out";
        Map<Character, Long> expected = new LinkedHashMap<>();
        expected.put('G', 5L);
        expected.put('e', 5L);
        expected.put('t', 10L);
        expected.put(' ', 9L);
        expected.put('o', 5L);
        expected.put('u', 5L);

        Map<Character, Long> actual = uniqueCharCounter.countCharacters(inputString);

        assertEquals(actual, expected);
    }

    @Test
    public void givenTextContainsSpecialSymbols_whenCountCharacter_thenSpecialSymbolsCountedCorrectly() {
        String inputString = "Gfd&ffd tt! ! gfd";
        Map<Character, Long> expected = new LinkedHashMap<>();
        expected.put('G', 1L);
        expected.put('f', 4L);
        expected.put('d', 3L);
        expected.put('&', 1L);
        expected.put(' ', 3L);
        expected.put('t', 2L);
        expected.put('!', 2L);
        expected.put('g', 1L);

        Map<Character, Long> actual = uniqueCharCounter.countCharacters(inputString);

        assertEquals(actual, expected);
    }

    @Test
    public void givenTextContainsMultipleSpaces_whenCountCharacter_thenMultipleSpacesCountedCorrectly() {
        String inputString = "   ! g    f";
        Map<Character, Long> expected = new LinkedHashMap<>();
        expected.put(' ', 8L);
        expected.put('!', 1L);
        expected.put('g', 1L);
        expected.put('f', 1L);

        Map<Character, Long> actual = uniqueCharCounter.countCharacters(inputString);

        assertEquals(actual, expected);
    }
}
