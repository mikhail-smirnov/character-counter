package com.mismirnov.character_counter;

import com.mismirnov.character_counter.counter.CachedUniqueCharCounter;
import com.mismirnov.character_counter.counter.Counter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CachedUniqueCharCounterTest {

    @InjectMocks
    private CachedUniqueCharCounter cachedUniqueCharCounter;

    @Mock
    private Counter counter;

    @Test
    public void givenTheSameTextInputTwice_whenCountCharacters_thenDelegatedCounterCalledOnce() {
        String inputString = "void";

        cachedUniqueCharCounter.countCharacters(inputString);
        cachedUniqueCharCounter.countCharacters(inputString);

        verify(counter, times(1)).countCharacters(any());
    }
}
