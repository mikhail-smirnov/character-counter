package com.mismirnov.character_counter.counter;

import java.util.Map;

public abstract class CounterDecorator implements Counter {

    private Counter counter;

    public CounterDecorator(Counter counter) {
        this.counter = counter;
    }

    @Override
    public Map<Character, Long> countCharacters(String inputString) {
        return counter.countCharacters(inputString);
    }
}
