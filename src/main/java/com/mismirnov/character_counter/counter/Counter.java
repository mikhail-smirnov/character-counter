package com.mismirnov.character_counter.counter;

import java.util.Map;

public interface Counter {

    Map<Character, Long> countCharacters(String inputString);
}
