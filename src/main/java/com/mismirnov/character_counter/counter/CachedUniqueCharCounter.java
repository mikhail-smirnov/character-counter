package com.mismirnov.character_counter.counter;

import java.util.HashMap;
import java.util.Map;

public class CachedUniqueCharCounter extends CounterDecorator {

    private Map<String, Map<Character, Long>> cache = new HashMap<>();

    public CachedUniqueCharCounter(Counter counter) {
        super(counter);
    }

    @Override
    public Map<Character, Long> countCharacters(String inputString) {
        return cache.computeIfAbsent(inputString, count -> super.countCharacters(inputString));
    }
}
