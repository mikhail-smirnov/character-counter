package com.mismirnov.character_counter.counter;

import java.util.*;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

public class UniqueCharCounter implements Counter {

    @Override
    public Map<Character, Long> countCharacters(String inputString) {
        if (inputString == null) {
            throw new IllegalArgumentException("String equals null");
        }
        return inputString.chars().mapToObj(character -> (char) character)
                .collect(groupingBy(Function.identity(),
                        LinkedHashMap::new,
                        counting()));
    }
}



