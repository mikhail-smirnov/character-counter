package com.mismirnov.character_counter;

import com.mismirnov.character_counter.counter.CachedUniqueCharCounter;
import com.mismirnov.character_counter.counter.UniqueCharCounter;

import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Type string: ");
        String inputString = scanner.nextLine();
        UniqueCharCounter uniqueCharCounter = new UniqueCharCounter();
        CachedUniqueCharCounter cachedUniqueCharCounter = new CachedUniqueCharCounter(uniqueCharCounter);
        Map<Character, Long> uniqueChars = cachedUniqueCharCounter.countCharacters(inputString);
        for (Map.Entry entry : uniqueChars.entrySet()) {
            System.out.println(entry.getKey() + (" : ") + entry.getValue());
        }
    }
}